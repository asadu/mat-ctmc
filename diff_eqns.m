%% Description
%  Creates the system of differential equation to calculate the transient state occupancy. As per equation(10) in the paper 
%  A. Sadu, G. K. Roy, F. Ponci and A. Monti, "Methodology for Reliability Analysis of Cyber-Physical MTdc Grids," in IEEE Journal of Emerging and Selected Topics in Power Electronics, doi: 10.1109/JESTPE.2020.2976748..  
%  Created by Abhinav Sadu 
%  Date : 30/05/2019

%% Function
function dpidt =diff_eqns(Q,pi_t,st_num)
    dpidt_1=zeros(st_num);
   
    for i=1:st_num 
        for j=1:st_num
            dpidt_1(i)=dpidt_1(i)+pi_t(j)*Q(j,i);            
        end
        if i == 1
            dpidt=[dpidt_1(1)];
        else
           dpidt=[dpidt;dpidt_1(i)]; 
        end
    end
     
    
end