%% Description
%  This calculates mean time spent in the transient states and mean time to
%  absorption. For single absorbing state the calculations are done according to  the equations (13)-(15) in the paper 
%  A. Sadu, G. K. Roy, F. Ponci and A. Monti, "Methodology for Reliability Analysis of Cyber-Physical MTdc Grids," in IEEE Journal of Emerging and Selected Topics in Power Electronics, doi: 10.1109/JESTPE.2020.2976748..  
%  For multiple absorbing states the time spent in transient states and time to absorption are calculated according to the equations (10.70)-(10.76) presented in the book 
%  Trivedi, K., 2017. Reliability and Availability Engineering : Modeling, Analysis, and Applications. 1st ed. Cambridge, United Kingdom: CAMBRIDGE UNIVERSITY PRESS.
%  Abhinav Sadu.
%  Created by Abhinav Sadu 
%  Date : 30/05/2019

%% Function

function [time_to_absorption,time_present_in_transient_states] = state_occupancy_time(Q,pi_0,st_num,ab_states)
%% Initializing the vectors 
time_to_absorption =zeros(1,ab_states);                                                                       % Initializing the time to absorption to absorbing state
pi_abs_infinity =zeros(1,ab_states);                                                                          % Initializing the final probability vector for the absorbing state
time_to_absorption_numerator=zeros(1,ab_states);                                                              % Initializing the numerator of time to absorption to absorbing state 
%% Generating matrices required for calculating time_to_absorption & time_present_in_transient_states
num_transient_states = st_num-ab_states;                                                                      % Calculate the umber of transient states        
Q_u=Q(1:num_transient_states,1:num_transient_states);                                                         % Create partition of Q for transient states
pi_0_u=pi_0(1:num_transient_states);                                                                          % Initial states of transient states
inv_Q_u=-inv(Q_u);                                                                                            % negative inverse of Q_u
e=ones(1,st_num-1);
if ab_states~=1    
    for i=1:ab_states
        pi_abs_infinity(i)=pi_0_u*inv_Q_u*Q(1:num_transient_states,num_transient_states+i);                   % Calculate the final probability of absorption to 'i'th absorption state 
    end
end
%% Calculation of time_present_in_transient_states before absorption
time_present_in_transient_states=pi_0_u*inv_Q_u;                                                              % Mean time spent in transient states

%% Calculation of time_to_absorption

if ab_states~=1
    for i=1:ab_states
        time_to_absorption_numerator(i)=time_present_in_transient_states*inv_Q_u*Q(1:num_transient_states,num_transient_states+i);% Calculate the numerator of time to absorption to 'i'th absorbing state
        time_to_absorption(i)=time_to_absorption_numerator(i)/pi_abs_infinity(i);                             % Mean time to absorbing in 'i'th absorbing state
    end
elseif ab_states==1
    time_to_absorption = pi_0_u*inv_Q_u*e';         % Mean time to absorption
end

