%% Description 
%  This is a function that computes the MTTF by integrating the reliability of the time horizon assumed for the simulation. It is based on the equation (2) in the paper  
%  A. Sadu, G. K. Roy, F. Ponci and A. Monti, "Methodology for Reliability Analysis of Cyber-Physical MTdc Grids," in IEEE Journal of Emerging and Selected Topics in Power Electronics, doi: 10.1109/JESTPE.2020.2976748.
%  Created by MSc.Abhinav Sadu 
%  Date : 22/06/2020

function[MTTF]=MTTF(t,data)
MTTF=trapz(t,data); %MTTF = integral  of Reliability from time 0 to infinity 
end