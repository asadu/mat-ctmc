%% Description 
%  This is a function that computes the steady state probability of state occupancy.
%  You can find the specific equation 9.33 in the book
%  Trivedi, K., 2017. Reliability and Availability Engineering : Modeling, Analysis, and Applications. 1st ed. Cambridge, United Kingdom: CAMBRIDGE UNIVERSITY PRESS.
%  Abhinav Sadu.
%  Considering all failure and restoration processes modeled in the CTMC being a random event with expnential PDF 
%  Created by MSc.Abhinav Sadu 
%  Date : 22/06/2020
function [steady_st_prob,mc_state]=steady_state_calculator(N_state,mc_state,Q)
b_1=zeros(N_state,1);
b_2=[1];
QQ= ones(1,N_state);
A=[Q' ; QQ];
b=[b_1;b_2];
steady_st_prob=inv(A'*A)*A'*b;
mc_state.steady_state_probability =steady_st_prob;
end

