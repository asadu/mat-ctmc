**Context**

This is a malab based tool for analysing CTMC. The Main.m is the main function to be run. 
An examplary CTMC with four states is provided with two possible failure configurations. 
One of the failure configuration corresponds to the non repairable system (specified in state_transition_CS.txt).
The other failure configuration corresponds to a completely repairable system ((specified in state_transition_CS_repair.txt))


**Steps to run the analysis tool : Example CTMC**

1) Make sure all teh files are in the same folder and open the Main.m in Matlab 2018 or above
2) Initialize the time horizon that is under study for calculating the transient probabilites within this time horizon
3) Initialize the number of absorbing states that are present in the markov chain. It should be noted that the absorbing states are indexed at the end of the states.txt.
4) Initialize the initial state vector under the subsection %% Initialization of CTMC states
5) Run the Main.m file in Matlab 2018 or above

**Steps to run the analysis tool : User defined CTMC**

1) Please update the state_CS_XX.txt and state_transition_CS_XX.txt with the details of your user defined CTMC model.
2) Make sure for every CTMC that needs to be evaluated there is this pair of state_XX.txt and state_transition_XX.txt defined and called in the Main.m 



**Description of the Main.m**
  
  This is the main function to load the CTMC and initialize them. With this
Main.m the transient probability of occupancy of different states, the steady state probability of occupying different states (completely repairable ),
time to reach an absorbing state, functional reliability of the system, MTTF associated to the functional reliability
are provided as an output. 

*The inputs to the Main.m*

      - states_CS_XX.txt (XX denotes a specific use-case identifier): The different states have to be enumerated in
        "states.txt" . The first coloumn corresponds to the state index
        number (integers) and the second coloumn is the name of the state (string)
        It should be noted that the absorbing states should be at the end.
      
      - state_transition_CS_XX.txt (XX denotes a specific use-case identifier) : the different state transitions of the CTMC 
        along with its transition rates are provided. Coloumn 1 and 2 correspond    
        to the state pair between which a transition is present.Coloumn 3 
        is the rate of transition and the coloumn 4 is the index of the transition 
        that would be used for assigning additional probabilities for a
        transition
        
*The outputs of the Main.m*

      - XX_mc_state (XX denotes a specific use-case identifier) :  
		It is a structure that has the following information.
        information on state names (state_name) and their index numbers
        (state_number) state transitions data (transition) and state
        occupancy probability (pi_t). "transition" has the data of the   
        transition (from and to state numbers) the rate of transition and
        the specific probabilities(probability) associated with each transition
        according to the index of the transition (probability_index).  
		
	  - XX_steady_state_prob(XX denotes a specific use-case identifier) : 
		It is defined only for completely repairable systems. 
		It is the state occupancy probability.
		For non repairable systems it is empty.  
     
      - pi_t : It is the state occupancy probability sored in an array 
        with the coloumn numbers corresponding to the state number.

      - Q : infitesmal generator  

      - time_to_absorb : Mean time taken by the system to 
        reach the absorbing state

      - time_in_transient : Mean time spent in the transient states
		before getting absorbed into the absorbing state
      
	  - R_XX (XX denotes a specific use-case identifier): 
		Functional reliability of the system. 
	  
	  - MTTF : Mean Time To Failure . Defined only for non repairable systems. 	
	  
*Functions*

      - CTMC_Analyser() : 
        Inputs --> path to state_XX.txt & state_transition_XX.txt, time_horizon,pi_0_XX(initialization of CTMC states), ab_states_XX(number of absorbing states)
        Outputs --> XX_mc_state (data structure with all CTMC indices),XX_steady_state_prob  
        Purpose --> Calculate all possible CTMC indices pertaining to repairable and non repairable systems.
					Mainly state occupancy probability, mean absorbption time, mean time spent in transient states
					store the data in structure XX_mc_state and provides XX_steady_state_prob

      - generate_state_datastructure() : 
        Inputs --> state_XX.txt & state_transition_XX.txt 
        Outputs --> Q (infitismal generator),N_state (scalar) 
        Purpose --> Stores the information in state_XX.txt and state_transition_XX.txt 
        in a data structure mc_state and
        provides N_state (number of states)

      - infitismal_generator() : 
        Inputs --> mc_state & probability
        Outputs --> Q (infitismal generator) 
        Purpose --> Generate the infitismal generator of the CTMC 

      - diff_eqns() : 
        Inputs --> Q, pi_0_XX (initial state), time_horizon(time horizon for transient CTMC evaluation), st_num ( number of states)
        Outputs --> pi_t (probabaility of state occupancy),t (time index for pi_t) 
        Purpose --> creates the system of differential equations for calculating 
                    the transient state occupancy
             
      - steady_state_calculator() : 
        Inputs --> XX_mc_state,Q, N_state
        Outputs --> mc_steady_state_prob & updated XX_mc_state
        Purpose --> Calculate the steady state probability of different states in a CTMC of a completely repairable system 

      - state_occupancy_time() : 
        Inputs --> Q,pi_0 (initial state occupancy probability),st_num (length(Q)),ab_states(number of absorbing states)
        Outputs --> time_to_absorption (mean time taken by the system to reach the absorbing state/s), 
                    time_present_in_transient_states (Mean time spent in transient states before being absorbed by the absorbing state)
        Purpose --> Calculate the mean absorption time & mean time spent in transient state before entering absorbing state
        
 **Sources of equations**
 
 The sources of the equations used to build the tool can be found in two sources. 
 1) A. Sadu, G. K. Roy, F. Ponci and A. Monti, "Methodology for Reliability Analysis of Cyber-Physical MTdc Grids," in IEEE Journal of Emerging and Selected Topics in Power Electronics, doi: 10.1109/JESTPE.2020.2976748.
    https://ieeexplore.ieee.org/document/9015968
 2) Trivedi, K., 2017. Reliability and Availability Engineering : Modeling, Analysis, and Applications. 1st ed. Cambridge, United Kingdom: CAMBRIDGE UNIVERSITY PRESS.