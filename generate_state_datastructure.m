function [N_state,mc_state]= generate_state_datastructure(file_path_state,file_path_state_transition)
%% Importing basic state data
%%% Initialize variables.
filename_state = file_path_state;
delimiter = '\t';
startRow = 2;

%%% Format for each line of text:
%   column1: double (%f)
%	column2: text (%s)
% For more information, see the TEXTSCAN documentation.
formatSpec_state = '%f%s%[^\n\r]';

%%% Open the text file.
fileID_state = fopen(filename_state,'r');

%%% Read columns of data according to the format.

dataArray = textscan(fileID_state, formatSpec_state, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

%%% Close the text file.
fclose(fileID_state);

%%% Create output variable
state = table2array(table(dataArray{1:end-1}, 'VariableNames', {'state_number','state_name'}));
N_state=length(state(:,1));

%%% Clear temporary variables
clearvars filename_state delimiter startRow formatSpec_state fileID_state dataArray ans;

%% Importing the trasition data
%%% Initialize variables.
filename = file_path_state_transition;
delimiter = '\t';
startRow = 2;

%%% Format for each line of text:
%   column1: double (%f)
%	column2: double (%f)
%   column3: double (%f)
%	column4: double (%f)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%f%f%f%f%[^\n\r]';

%%% Open the text file.
fileID = fopen(filename,'r');

%%% Read columns of data according to the format.
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'HeaderLines' ,startRow-1, 'ReturnOnError', false, 'EndOfLine', '\r\n');

%%% Close the text file.
fclose(fileID);

%%% Create output variable
statetransition = table2array(table(dataArray{1:end-1}, 'VariableNames', {'state_from','state_to','transition_rate','Probability_index'}));

%%% Clear temporary variables
clearvars filename delimiter startRow formatSpec fileID dataArray ans;

%% Creating state data structure
mc_state.state_number = state(:,1);
mc_state.state_name =state(:,2);
mc_state.transition.state_from=statetransition(:,1);
mc_state.transition.state_to=statetransition(:,2);
mc_state.transition.rate=statetransition(:,3);
mc_state.transition.probability_index=statetransition(:,4);
end
