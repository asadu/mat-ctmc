%% Description 
%   main file to evaluate the CTMC
%
%  Created by MSc.Abhinav Sadu 
%  Date : 07/07/2019

clear all;
clc;

%% Initialize the time horizon
time_horizon = [0:.1:5];                                                                                                    % time_horizon = [start_time end_time]

%% Loading inputs state.txt & state_transition.txt for each CTMC

%%% Load CTMC states and state transition txt files
state_CS= 'state_CS.txt';
state_transition_CS= 'state_transition_CS.txt';
state_transition_CS_repair= 'state_transition_CS_repair.txt';



%% Initializing number of absorbing states
ab_states_CS_Failure=1;                                                                                                     % Number of absorbing states for CTMC without repair
ab_states_CS_Failure_repair=0;                                                                                              % Number of absorbing states for CTMC with repair

%% Initialization of CTMC states
pi_0_CS=[1 0 0 0 ]; 

%% CTMC evaluation of non repairable system
                                                                                                        % Initial state vector
[CS_mc_state,CS_steady_state_prob]=CTMC_Analyser(state_CS,state_transition_CS,time_horizon,pi_0_CS,ab_states_CS_Failure);   % Calculate CTMC indices for non repairable system
[~,num_CS_state]=size(CS_mc_state.pi_t);                                                                                    % Obtaining the size of pi_t to get the number of CTMC states

%% Additional performance indices calculable for repairable systems (MTTF, Functional Reliability)
% Extract the time steps 
time_steps=length(CS_mc_state.t);
t= CS_mc_state.t;
% Define the performance classes (which states are considered full operational mode)
pc_index=[1,3];                                                                                                             % Index number of the state mentioned in state.txt
[pc_num,~]=size(pc_index);                                                                                                  % Total number of states considered as full operational

% Calculation of the functonal reliability and MTTF (Mean Time To Failure)
R_CS=zeros(time_steps,1);                                                                                                   % Initialization of functional reliability vector 
for i= 1:num_CS_state
        if ismember(i,pc_index)
        R_CS(:,1)=R_CS(:,1)+CS_mc_state.pi_t(:,i);                                                                          % Calculation of functional reliability
        end
end
MTTF_System=MTTF(t,R_CS);                                                                                                   % Calculation of MTTF

figure()
plot(t,R_CS)
title('Functional Reliability')
xlabel('Time (Hours)')
ylabel('Reliability')
txt = ['MTTF: ' num2str(MTTF_System) ' Time Units'];
text(1.5,0.9,txt)

%% CTMC evaluation of completely repairable system
[CS_mc_state_repair,CS_steady_state_prob_repair]=CTMC_Analyser(state_CS,state_transition_CS_repair,time_horizon,pi_0_CS,ab_states_CS_Failure_repair);
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             
                                                             

