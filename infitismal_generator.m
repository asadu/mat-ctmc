%% Description
%  This calculates infitismal generator As per equation(11) in the paper 
%  A. Sadu, G. K. Roy, F. Ponci and A. Monti, "Methodology for Reliability Analysis of Cyber-Physical MTdc Grids," in IEEE Journal of Emerging and Selected Topics in Power Electronics, doi: 10.1109/JESTPE.2020.2976748..  
%  Created by Abhinav Sadu 
%  Date : 30/05/2019

%% Function
function Q = infitismal_generator(mc_state,probability)

transition_num =length(mc_state.transition.state_from);     % retreive number of transitions
state_num = length(mc_state.state_number);                  % retreive number of states

state_from =mc_state.transition.state_from;                 % retreive number of transition state from data
state_to =mc_state.transition.state_to;                     % retreive number of transition state to data
transition_rate=mc_state.transition.rate;                   % retreive number of transition rate data
%% Creating Q matirx (infitisimal generator)
Q=zeros(state_num,state_num);

%%% Stamp the off diagonal elements
    for i= 1: transition_num
        Q(state_from(i),state_to(i))=Q(state_from(i),state_to(i))+transition_rate(i)*probability(i);
    end
 
%%% Stamp the diagonal elements
    for i= 1: state_num
        for j= 1:state_num
            if (i~=j)
                Q(i,i)=Q(i,i)-(Q(i,j));
            end
        end
    end

end