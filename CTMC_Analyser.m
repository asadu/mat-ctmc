%% Description 
%  This is a function that computes the dynamic state occupancy probability and 
%  other reliability indices like, Mean absorption time, time spent in transient states.  
%  Considering all failure and restoration processes modeled in the CTMC being a random event with expnential PDF 
%  Created by MSc.Abhinav Sadu 
%  Date : 22/06/2020

function[mc_state,mc_steady_state_prob]=CTMC_Analyser(file_path_state,file_path_state_transition,time_horizon,pi_0,ab_states)
%% Initialization

%%% generating data structure for CTMC evaluation
[N_state,mc_state]= generate_state_datastructure(file_path_state,file_path_state_transition);

%%% Initialize Probabilities 
% Uncomment the line 15 and provide the probability vector where the row  
% number corresponds to the transition number as mentioned in state_transition.txt

% probability = [0;0.6;0.3;0.1]

%%% Intialization of probabilities 
% Comment lines 22 and 23 if there exists a probability vector as defined
% in Line 18. Else keep it as it is.


num_transitions = length(mc_state.transition.probability_index);                                            % number of transitions in CTMC
probability=ones(num_transitions,1);                                                                        % initializes probability vector 

mc_state.transition.probability=probability(:,1);                                                           % storing the probability vector into the data structure

%% Calculation of the infitismal genertor Q
Q = infitismal_generator(mc_state,probability);                                                             % calculation of Q
st_num=length(Q);                                                                                           % number of states

%% Reliability indices for completely repairable systems
% Calculate steady state probabilities of completely repairable systems 
[mc_steady_state_prob,mc_state]=steady_state_calculator(N_state,mc_state,Q);
[t,pi_t]=ode23(@(t,pi_t)diff_eqns(Q,pi_t,st_num),time_horizon,pi_0);                                    % Calculation of transient(dynamic) state occupancy
mc_state.pi_t=pi_t;                                                                                     % storing pi_t into data structure
mc_state.t=t;

%% Reliability indices for non repairable systems
if(ab_states~=0)
    mc_steady_state_prob=[];                                                                                 % Non repairable systems will have probability 1 to gfail as t--> infinity
    % Transient state occupancy probability calculation for non repairable systems
    [t,pi_t]=ode23(@(t,pi_t)diff_eqns(Q,pi_t,st_num),time_horizon,pi_0);                                    % Calculation of transient(dynamic) state occupancy
    mc_state.pi_t=pi_t;                                                                                     % storing pi_t into data structure
    mc_state.t=t;                                                                                           % storing t into data structure

    % Mean time to absorption & mean time spent in transient states
    [time_to_absorption,time_present_in_transient_states] = state_occupancy_time(Q,pi_0,st_num,ab_states);
    mc_state.time_to_absorption=time_to_absorption;                                                         % storing time_to_absorption into data structure
    mc_state.time_present_in_transient_states=time_present_in_transient_states;  
end
%% Plots 


% Initializing the different plots
if(ab_states~=0)
    figure()
    for i = 1:st_num
        plot(t,pi_t(:,i));
        hold on;
        legeninfo{i}=mc_state.state_name(i);
    end
legend(legeninfo);
title('State occupancy probability')
xlabel('Time (Time Units)')
ylabel('Probability')
    
figure()
T_spent=time_present_in_transient_states;
bar(T_spent,0.4)
title('Mean time spent in transient state')
ylabel('Expected time spent (Time Units)')
xlabel('Transient States')
ylim([0 1.2*max(T_spent)])
set(gca, 'xtickLabel',legeninfo(1:st_num-1))
text(1:length(T_spent),T_spent,num2str(round(T_spent,3,'significant')'),'vert','bottom','horiz','center');
grid on

figure()
T_spent_abs=time_to_absorption';
bar(T_spent_abs)
title('Mean time to absorption')
ylabel('Expected time to absorption(Time Units)')
ylim([0 1.2*max(T_spent_abs)])
set(gca,'xticklabel',[legeninfo{((st_num - ab_states)+1):st_num}]);
text(1:length(T_spent_abs),T_spent_abs,num2str(round(T_spent_abs,4,'significant')'),'vert','bottom','horiz','center');
grid on
else
    figure()
    % creating the legends automatically according to the state names given in
% state.txt
    for i = 1:st_num
        legeninfo{i}=mc_state.state_name(i);
    end
    d=mc_steady_state_prob';
    bar(d,0.3)
    title('Steady State Probability')
    ylabel('Probability')
    ylim([0 1.2*max(d)])
    set(gca,'xticklabel',[legeninfo{1:st_num}]);
    if st_num>4
    xtickangle(-45)
    end
    text(1:length(d),d,num2str(round(d,2,'significant')'),'vert','bottom','horiz','center');
    grid on
end

end
